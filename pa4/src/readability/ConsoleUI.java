package readability;

import java.io.File;
import java.util.Iterator;
import java.util.Scanner;
/**
 * The console UI of the text or book readability
 * @author Parinvut Rochanavedya
 * @version 28-03-2015
 */
public class ConsoleUI {
	/**
	 * Main class, works much like cmd.exe
	 * @param args : does nothing
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		File currentDefaultFilePath = new File("c:\\");
		if(!currentDefaultFilePath.isDirectory()) return;
		while(true){
			String currentPath = currentDefaultFilePath.getAbsolutePath();
			System.out.print(currentPath+"> ");
			String commandLine = scanner.nextLine();
			Iterator<String> commandScan = new Scanner(commandLine);
			if(!commandScan.hasNext()){
				continue;
			} else {
				String command = commandScan.next();
				if(command.equalsIgnoreCase("exit")){
					System.exit(0);
					break;
				} 
				if(command.equalsIgnoreCase("mkdir")){
					if(!commandScan.hasNext()){
						System.out.println("Make Directory Error: please type valid folder name");
						continue;
					} 
					continue;
				}
				if(command.equalsIgnoreCase("cd")){
					if(!commandScan.hasNext()){
						System.out.println("Change Directory Error: please type valid path");
						continue;
					}
					continue;
				}
				if(command.equalsIgnoreCase("java")){
					if(!commandScan.hasNext()){
						System.out.println("Java by Oracle TM");
						System.out.println("<<commands>>");
						System.out.println("-readability  -- calculate the flesch readability index of the given file path or url");
						System.out.println("              -> java readability <file path or URL>");
						continue;
					}
					command = commandScan.next();
					if(command.equalsIgnoreCase("readability")){
						if(commandScan.hasNext()){
							String path = commandScan.next();
							String abs_path = currentPath+path;
	
							if(Readability.isURL(path))
								abs_path = path;
							
							int syllables = Readability.countTotalSyllables(abs_path);
							int words = Readability.countTotalWords(abs_path);
							int sentences = Readability.countTotalSentences(abs_path);
							double fRI = Readability.getFleschReadabilityIndex(syllables, words, sentences);
							String level = Readability.compareReaderLevel(fRI);
							
							if(syllables == -1 || words == -1 || sentences == -1){
								System.out.println("Java Readability Error: no valid file path or URL given");
								continue;
							}
							
							System.out.print("File name: ");
							System.out.println(path);
							System.out.print("Total syllables: ");
							System.out.println(syllables);
							System.out.print("Total words: ");
							System.out.println(words);
							System.out.print("Total sentences: ");
							System.out.println(sentences);
							System.out.print("Flesch Readability Index: ");
							System.out.printf("%.1f\n",fRI);
							System.out.print("Reader's Level: ");
							System.out.println(level);
							continue;
						}
						System.out.println("Java Readability Error: no valid file path or URL given");
						continue;
					}
					System.out.println("\""+command+"\" is not a valid Java command.");
					continue;
				}
				System.out.println("\""+command+"\" is not a valid command.");
			}
		}
	}

}
