package readability;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Word Counter class counts syllables in words of String
 * If there's any syllables and it's a valid of a word, count it as a word.
 * @author Parinvut Rochanavedya
 * @version 28-03-2015
 */
public class WordCounter {
	/**
	 * States
	 */
	private int state;
	private final int START = 0;
	private final int CONSONANT = 1;
	private final int VOWEL = 2;
	private final int FINAL_E_CHECK = 3;
	private final int NONWORD = 4;
	
	final String ignoreListAtStart = "'\"({[";
	final String ignoreListAtEnd = "'\",.!?)}];:";
	
	/**
	 * Count the syllables of the given word
	 * @param word : the word
	 * @return > 0 if it is a word
	 * 			0 if it is not a word
	 */
	public int countSyllables(String word){
		int syllables = 0;
		state = START;
		for(int i = 0; i <= word.length(); i++){ //the "i" is the index of the letter to be checked
			char c = 0; //set to space-bar at first
			boolean wordEnds = i == word.length();
			if(!wordEnds)
				c = word.charAt(i);
			//ignore the ignore list
			if(c == '\''){ //contains '
				continue;
			}
			if(ignoreListAtStart.contains(c+"") && i == 0){
				//check dash 
				if(word.charAt(1) == '-')
					state = NONWORD;
				continue;
			}
			if(ignoreListAtEnd.contains(c+"") && i == word.length()-1 && word.length() > 1){
				//check dash
				if(word.charAt(word.length()-2) == '-')
					state = NONWORD;
				continue;
			}
			//[(word).] ["word":] ["word."] fix
			if(c == ')' && word.charAt(i+1) == '.'){
				continue;
			}
			if(c == '"' && word.charAt(i+1) == ':'){
				continue;
			}
			if(c == '.' && word.charAt(i+1) == '"'){
				continue;
			}
			//LETTER STATE MACHINE
			switch(state){
			case START: 
				if(isVowel(c)||c=='y'||c=='Y'){
					state = VOWEL;
					break;
				}
				if(Character.isLetter(c)){
					state = CONSONANT;
					break;
				}
				state = NONWORD;
				break;
			case CONSONANT:
				if(c=='e'||c=='E'){
					state = FINAL_E_CHECK;
					break;
				}
				if(isVowel(c)||c == 'y'||c == 'Y'){
					state = VOWEL;
					break;
				}
				if(Character.isLetter(c)||(c=='-' && i != word.length()-1)){
					state = CONSONANT;
					break; //stay
				}
				if(wordEnds){
					break;
				}
				state = NONWORD;
				break;
			case VOWEL:
				if(isVowel(c)){
					state = VOWEL;
					break; //stay
				}
				if(Character.isLetter(c)||(c=='-' && i != word.length()-1)){
					state = CONSONANT;
					syllables++; //count
					break;
				}
				if(wordEnds){
					syllables++;
					break;
				}
				state = NONWORD;
				break;
			case FINAL_E_CHECK:
				if(isVowel(c)){
					state = VOWEL;
					break;
				}
				if(Character.isLetter(c)||(c=='-' && i != word.length()-1)){
					state = CONSONANT;
					syllables++; //count
					break;
				}
				if(wordEnds && syllables > 0){
					break; //it is final e and syllable is more than 1
				}
				if(wordEnds && syllables == 0){
					syllables++;
					break;//it is final e but syllable is zero
				}
				state = NONWORD;
				break;
			case NONWORD:
				syllables = 0;
				break;
			}
		}
		return syllables;
	}
	/**
	 * Count the words of the given file url.
	 * @param url : the given file link to read by counting words in it
	 * @return > 0 if there's a word
	 * 			0 if no words
	 * 			-1 if it's unreadable
	 * @throws IOException 
	 */
	public int countWords(String url){
		int words = 0;
		File file = new File(url);
		try{
			Scanner scanner = null;
			if(Readability.isURL(url)){
				URL url_file = new URL(url);
				InputStream input = url_file.openStream();
				scanner = new Scanner(input);
			} else {
				scanner = new Scanner(file);
			}
			while(scanner.hasNext()){
				if(countSyllables(scanner.next()) > 0){
					words++;
				}
			}
		} catch (IOException e){
			words = -1;
		}
		return words;
	}
	/**
	 * Check if the letter is one of the vowel letters
	 * known as A E I O U
	 * @param c : the given character
	 * @return true if it's one of the vowel letters
	 */
	public boolean isVowel(char c){
		return c == 'a' || c == 'A' || c == 'e' || c == 'E' || c == 'i' || c == 'I' || 
				c == 'o' || c == 'O' || c == 'u' || c == 'U';
	}
	
}
