package readability;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.CardLayout;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JEditorPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * The graphical UI of the text or book readability
 * @author Parinvut Rochanavedya
 * @version 28-03-2015
 */
public class GraphicalUI extends JFrame {
	/**Serial Version*/
	private static final long serialVersionUID = 1L;
	/**
	 * Components
	 */
	private JPanel contentPane;
	private JTextField url_file_text;
	private JFileChooser chooser;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GraphicalUI frame = new GraphicalUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GraphicalUI() {
		setTitle("File Reader");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 630, 310);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lbl_description = new JLabel("Open file or URL:");
		
		chooser = new JFileChooser();
		chooser.setFileFilter(new FileNameExtensionFilter("Text Files (.txt)","txt"));
		
		url_file_text = new JTextField();
		url_file_text.setColumns(10);
		
		chooser.removeChoosableFileFilter(chooser.getAcceptAllFileFilter());
		
		JButton btn_browse = new JButton("Browse...");
		
		
		JButton btn_count = new JButton("Count");
		
		
		JButton btn_clear = new JButton("Reset");
		
		JEditorPane results = new JEditorPane();
		results.setEditable(false);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lbl_description)
							.addGap(18)
							.addComponent(url_file_text, GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE)
							.addGap(32)
							.addComponent(btn_browse)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btn_count)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btn_clear))
						.addComponent(results, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 500, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(url_file_text, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lbl_description)
						.addComponent(btn_clear)
						.addComponent(btn_count)
						.addComponent(btn_browse))
					.addGap(18)
					.addComponent(results, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
		
		btn_browse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openFile(e);
				results.setText("");
			}
		});
		
		btn_count.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String path = url_file_text.getText();

				String result = "";
				
				int syllables = Readability.countTotalSyllables(path);
				int words = Readability.countTotalWords(path);
				int sentences = Readability.countTotalSentences(path);
				double fRI = Readability.getFleschReadabilityIndex(syllables, words, sentences);
				String level = Readability.compareReaderLevel(fRI);
				
				if(syllables == -1 || words == -1 || sentences == -1){
					result = "Java Readability Error: no valid file path or URL given";
				}
				else {
					result += "File name: ";
					result += path+"\n";
					result += "Total syllables: ";
					result += syllables+"\n";
					result += "Total words: ";
					result += words+"\n";
					result += "Total sentences: ";
					result += sentences+"\n";
					result += "Flesch Readability Index: ";
					result += String.format("%.1f\n",fRI)+"\n";
					result += "Reader's Level: ";
					result += level+"\n";
				}

				results.setText(result);
			}
		});
		
		btn_clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				results.setText("");
			}
		});
		
	}
	
	private void openFile(ActionEvent evt) {
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
           url_file_text.setText(chooser.getSelectedFile().getAbsolutePath());
        }
    }
}
