package readability;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

/**
 * Readability class reads the file and
 * indentifies the reader's level.
 * The class implements word-counter class to count syllables and words
 * @author Parinvut Rochanavedya
 * @version 28-03-2015
 */
public class Readability {
	/**Word Counter*/
	private static final WordCounter wordCounter = new WordCounter();
	/**
	 * Application method
	 * @param args : if it contains strings, run console
	 * 				 but if it's not, run GUI
	 */
	public static void main(String[] args) {
		//check args[]
		if(args.length > 0){
			//run console
			ConsoleUI.main(args);
		} else {
			//run GUI
			GraphicalUI.main(args);
		}
	}
	/**
	 * Call the instance of the word-counter
	 * @return the word-counter
	 */
	public static final WordCounter getWordCounter(){
		return wordCounter;
	}
	/**
	 * Calculate the flesch readability index by it's own formula
	 * @param syllables
	 * @param words
	 * @param sentences
	 * @return flesch readability index
	 */
	public static double getFleschReadabilityIndex(int syllables, int words, int sentences){
		if(words == 0 || sentences == 0){return Double.NEGATIVE_INFINITY;}
		return 206.835 - 84.6*((double)syllables/(double)words)- 1.015*((double)words/(double)sentences);
	}
	/**
	 * Compare the level of the reader with the given flesch readability index
	 * @return reader's level
	 */
	public static String compareReaderLevel(double fleschReadabilityIndex){
		double i = fleschReadabilityIndex;
		if(i > 100){
			return "4th Grade Student";
		}
		else if(i > 90){
			return "5th Grade Student";
		}
		else if(i > 80){
			return "6th Grade Student";
		}
		else if(i > 70){
			return "7th Grade Student";
		}
		else if(i > 65){
			return "8th Grade Student";
		}
		else if(i > 60){
			return "9th Grade Student";
		}
		else if(i > 50){
			return "High School Student";
		}
		else if(i > 30){
			return "College Student";
		}
		else if(i > 0){
			return "College Graduate";
		}
		else if (i == Double.NEGATIVE_INFINITY){
			return "NOT A BOOK!! No words at all!!"; 
		}
		else return "Advanced Degree Graduate";
	}
	/**
	 * Get all the protocols to make a valid URL
	 */
	private static final String[] validUrlProtocols = new String[]{"http","https","file","ftp"};
	public static final String[] getValidUrlProtocols(){
		return validUrlProtocols;
	}
	/**
	 * Check if the givven string is url
	 * @param url : target path for text or book
	 * @return true if the protocol is valid
	 * 			false if it's something else
	 */
	public static boolean isURL(String url){
		if(!url.contains(":")){return false;}
		boolean isURL = false;
		for(int i = 0; i < getValidUrlProtocols().length; i++){
			if(url.contains(getValidUrlProtocols()[i])){
				isURL = true;
				break;
			}
		}
		return isURL;
	}
	/**
	 * Count total syllables in a file or URL of a text or book
	 * @param url : target path for text or book
	 * @return total syllables
	 */
	public static int countTotalSyllables(String url){
		int syllables = 0;
		File file = new File(url);
		try{
			Scanner scanner = null;
			if(isURL(url)){
				URL url_file = new URL(url);
				InputStream input = url_file.openStream();
				scanner = new Scanner(input);
			} else {
				scanner = new Scanner(file);
			}
			while(scanner.hasNext()){
				syllables += getWordCounter().countSyllables(scanner.next());
			}
		} catch (IOException e){
			syllables = -1;
		}
		return syllables;
	}
	/**
	 * Count total words in a file or URL of a text or book
	 * @param url : target path for text or book
	 * @return total words
	 */
	public static int countTotalWords(String url){
		return getWordCounter().countWords(url);
	}
	/**
	 * Count total sentences in a file or URL of a text or book
	 * @param url : target path for text or book
	 * @return total sentences
	 */
	public static int countTotalSentences(String url){
		int sentences = 0;
		File file = new File(url);
		try{
			Scanner scanner = null;
			if(isURL(url)){
				URL url_file = new URL(url);
				InputStream input = url_file.openStream();
				scanner = new Scanner(input);
			} else {
				scanner = new Scanner(file);
			}
			int currentWordsInSentence = 0;
			while(scanner.hasNextLine()){
				String currentLine = scanner.nextLine();
				Scanner scannerForEachLine = new Scanner(currentLine);
				if(!scannerForEachLine.hasNext()){
					sentences--; //blank line doesn't count
				}
				while(scannerForEachLine.hasNext()){
					String mostRecentWord = scannerForEachLine.next();
					if(getWordCounter().countSyllables(mostRecentWord) > 0){
						currentWordsInSentence++;
					}
					if(mostRecentWord.endsWith(".") || mostRecentWord.endsWith(";")
							|| mostRecentWord.endsWith("?") || mostRecentWord.endsWith("!")
							|| mostRecentWord.endsWith(".\"") //dot inside quotation case
							|| mostRecentWord.endsWith("\n\\n")){
						if(currentWordsInSentence > 0){
							sentences++;
						}
						currentWordsInSentence = 0;
					}
					if(!scannerForEachLine.hasNext() && currentWordsInSentence > 0){
						sentences++;
					}
				}
			}
		} catch (IOException e){
			sentences = -1;
		}
		return sentences;
	}
	
}
